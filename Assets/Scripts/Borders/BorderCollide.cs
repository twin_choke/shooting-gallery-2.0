﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderCollide : MonoBehaviour
{
    public event Action<Vector3> TargetCollide;
    [SerializeField] Direction direction;

    public enum Direction
    {
        Right,
        Left
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Target"))
        {
            switch (direction)
            {
                case Direction.Right:
                    TargetCollide?.Invoke(Vector3.left);
                    break;

                case Direction.Left:
                    TargetCollide?.Invoke(Vector3.right);
                    break;
            }
        }
    }
}

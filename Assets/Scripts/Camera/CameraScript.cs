﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] [Range(30, 90)] private float mouseSens;
    [SerializeField] private float xAngleLimit;
    [SerializeField] private float yAngleLimit;
    [HideInInspector] public bool isCameraMoving = false;

    Transform camTransform;
    Vector3 rotation;
    float startXrotation;

    private void Start()
    {
        camTransform = Camera.main.transform;
        Cursor.visible = false;
        rotation = transform.rotation.eulerAngles;
        startXrotation = rotation.x;
    }

    void LateUpdate()
    {
        if (isCameraMoving)
            LookAround();
    }

    void LookAround()
    {
        float rotateX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float rotateY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        rotation.x -= rotateY;
        rotation.y += rotateX;

        rotation.x = Mathf.Clamp(rotation.x, -yAngleLimit + startXrotation, yAngleLimit + startXrotation);
        rotation.y = Mathf.Clamp(rotation.y, -xAngleLimit, xAngleLimit);

        camTransform.eulerAngles = rotation;
    }
}

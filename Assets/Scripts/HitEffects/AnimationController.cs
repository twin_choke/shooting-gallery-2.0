﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class AnimationController : MonoBehaviour
    {
        Animator animator;

        public void PlayAnimation()
        {
            animator = GetComponent<Animator>();
            gameObject.SetActive(true);
            animator.Play("WowAnim", 0);
            float animLength = animator.GetCurrentAnimatorStateInfo(0).length;
            StartCoroutine(RemoveAfterSeconds(animLength));
        }

        IEnumerator RemoveAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            gameObject.SetActive(false);
        }
    }
}

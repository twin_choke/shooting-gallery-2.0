﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class BaseHit
    {
        AudioSource source;
        AudioClip hitSound;       

        public BaseHit(AudioSource source, AudioClip hitSound)
        {
            this.hitSound = hitSound;
            this.source = source;
        }

        public virtual void HitCongratulation(GameObject targ)
        {
            source.clip = hitSound;
            source.Play();
        }
    }
}

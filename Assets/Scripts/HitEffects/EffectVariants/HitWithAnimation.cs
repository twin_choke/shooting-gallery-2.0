﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class HitWithAnimation : BaseHit
    {
        AnimationController controller;

        public HitWithAnimation(AudioSource source, AudioClip hitSound, GameObject congratGameObj) : base(source, hitSound)
        {
            controller = congratGameObj.GetComponent<AnimationController>();
        }

        public override void HitCongratulation(GameObject targ)
        {
            base.HitCongratulation(targ);
            controller.PlayAnimation();
        }
    }
}
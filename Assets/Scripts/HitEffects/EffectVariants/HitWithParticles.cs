﻿using UnityEngine;

namespace Effects
{
    public class HitWithParticles : BaseHit
    {
        ParticleSystem particles;

        public HitWithParticles(AudioSource source, AudioClip hitSound, ParticleSystem particles) : base(source, hitSound)
        {
            this.particles = particles;
        }

        public override void HitCongratulation(GameObject targ)
        {
            base.HitCongratulation(targ);
            particles.transform.position = targ.transform.position;
            particles.Play();
        }
    }
}
﻿using UnityEngine;

[CreateAssetMenu(fileName = "EffectsResourceSet", menuName = "EffectsResourceContainer")]
public class EffectsResourceContainer : ScriptableObject
{
    [SerializeField] AudioClip simpleHitSound;
    [SerializeField] AudioClip hitWithParticlesSound;
    [SerializeField] AudioClip hitWithAnimationSound;
    [SerializeField] ParticleSystem particles;
    [SerializeField] GameObject animationObject;

    public AudioClip SimpleHitSound => simpleHitSound;
    public AudioClip HitWithParticlesSound => hitWithParticlesSound;
    public AudioClip HitWithAnimationSound => hitWithAnimationSound;
    public ParticleSystem Particles => particles;
    public GameObject AnimationObject => animationObject;
}

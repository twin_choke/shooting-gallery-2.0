﻿using UnityEngine;

namespace Effects
{
    public class HitContext // класс контекста в паттерне Стратегия
    {
        BaseHit hitEffect;

        public void SetEffect(BaseHit hitEffect) => this.hitEffect = hitEffect;

        public void RunHitEffect(GameObject targ) => hitEffect.HitCongratulation(targ);
    }
}
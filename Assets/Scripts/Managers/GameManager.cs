﻿using IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Targets;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public event Action<bool> BlockPlayerAction;
    /// <summary>
    /// Ключ - имя мишени, значение - награда. 
    /// </summary>
    public Dictionary<string, int> RewardDict { get; private set; } = new Dictionary<string, int>();
    public static GameManager instance;
    List<GameObject> TargetPrefabs = new List<GameObject>();
    List<GameObject> MenuItems = new List<GameObject>();

    [SerializeField] GameObject menuItemsParent;
    [SerializeField] GameObject menuItemPrefab;
    [SerializeField] float highscoreShowTime;
    TargetDataContainer container;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        LoadTargetData();
        TargetManager.instance.Init(TargetPrefabs);
        UIManager.instance.Init(RewardDict);
    }

    private void Start()
    {
        StartCoroutine(GameLoop());
    }

    void LoadTargetData()
    {
        container = XMLSerializer.Load(Application.streamingAssetsPath + "/TextData/TargetsData.xml");
        var orderedData = container.TargetsDataList.OrderBy(el => el.reward);

        foreach (var targData in orderedData)
        {
            RewardDict.Add(targData.targetName, targData.reward);

            var targetPrefab = Resources.Load<GameObject>(targData.targetName);
            TargetPrefabs.Add(targetPrefab);

            var menuItemInstance = Instantiate(menuItemPrefab, menuItemsParent.transform, false);
            var menuItemSprite = menuItemInstance.transform.Find("Icon").gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(targData.iconName);
            menuItemInstance.GetComponentInChildren<Text>().text = targData.reward.ToString();
            menuItemInstance.GetComponentInChildren<Button>().onClick.AddListener(new UnityAction(() => TargetManager.instance.AddTargetToScene(targData.targetName)));
            MenuItems.Add(menuItemInstance);
        }
    }

    IEnumerator GameLoop()
    {
        yield return StartCoroutine(GameStart());
        yield return StartCoroutine(GameRun());
        Application.Quit();
    }

    IEnumerator GameStart()
    {
        UIManager.instance.ShowHighscore(true);
        UIManager.instance.enabled = false;
        BlockPlayerAction?.Invoke(true);
        yield return new WaitForSeconds(highscoreShowTime);
    }

    IEnumerator GameRun()
    {
        UIManager.instance.enabled = true;
        UIManager.instance.ShowHighscore(false);
        BlockPlayerAction?.Invoke(false);

        while (!Input.GetKeyDown(KeyCode.Q))
        {
            yield return null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shooting;
using Targets;

public class PermissionsManager : MonoBehaviour
{
    [SerializeField] ShootingScript shooting;
    [SerializeField] CameraScript camScript;
    public static PermissionsManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        UIManager.instance.MenuOpened += HandleMenuOpening;
        GameManager.instance.BlockPlayerAction += BlockPlayerActions;
    }

    void HandleMenuOpening(bool isMenuOpened)
    {
        shooting.isShootingEnabled = camScript.isCameraMoving = !isMenuOpened;
        Cursor.visible = isMenuOpened;
    }

    void BlockPlayerActions(bool blocked) => shooting.isShootingEnabled = camScript.isCameraMoving = TargetManager.instance.isTargetMoveAllowed = UIManager.instance.isMenuOpeningAllowed = !blocked;
}
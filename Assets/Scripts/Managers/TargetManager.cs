﻿using Effects;
using System.Collections.Generic;
using UnityEngine;

namespace Targets
{
    public class TargetManager : MonoBehaviour
    {
        public TargetsObserver observer;
        public static TargetManager instance;
        [HideInInspector] public bool isTargetMoveAllowed = false;

        [SerializeField] TargetCreate targCreate;
        [SerializeField] TargetMove targMove;
        [SerializeField] Canvas canvas;
        [SerializeField] GameObject dynamicObjParent;
        [SerializeField] EffectsResourceContainer hitEffects;

        List<GameObject> TargetPrefabs;
        GameObject[] SceneTargets;     

        AudioSource audioSource;
        HitContext hitContext;
        BaseHit baseHit;
        HitWithAnimation animationHit;
        HitWithParticles particlesHit;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        public void Init(List<GameObject> TargetPrefabs)
        {
            InitData(TargetPrefabs);
            Config();
        }

        void InitData(List<GameObject> TargetPrefabs)
        {
            this.TargetPrefabs = TargetPrefabs;

            observer = new TargetsObserver();

            hitContext = new HitContext();
            audioSource = GetComponent<AudioSource>();
            baseHit = new BaseHit(audioSource, hitEffects.SimpleHitSound);
            var animation = Instantiate(hitEffects.AnimationObject, canvas.transform);
            animationHit = new HitWithAnimation(audioSource, hitEffects.HitWithAnimationSound, animation);
            var particles = Instantiate(hitEffects.Particles, dynamicObjParent.transform);
            particlesHit = new HitWithParticles(audioSource, hitEffects.HitWithParticlesSound, particles);
        }

        void Config()
        {
            observer.HitResponse += OnTargetHitCongratulation;
            targCreate.Init(TargetPrefabs, observer);
            targCreate.SetSceneTargets();
            SceneTargets = targCreate.SceneTargets;

            targMove.Init(SceneTargets);
        }

        void FixedUpdate()
        {
            if (isTargetMoveAllowed)
                targMove.MovePosition();
        }

        public void AddTargetToScene(string prefabName) => targCreate.AddToScene(prefabName);

        // паттерн Стратегия
        void OnTargetHitCongratulation(GameObject targ)
        {
            if (targ.CompareTag("TextCongrat"))
                hitContext.SetEffect(animationHit);

            else if (targ.CompareTag("ParticlesCongrat"))
                hitContext.SetEffect(particlesHit);

            else hitContext.SetEffect(baseHit);

            hitContext.RunHitEffect(targ);
        }
    }
}


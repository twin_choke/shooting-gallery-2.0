﻿using System;
using System.Collections.Generic;
using Targets;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [HideInInspector] public bool isMenuOpeningAllowed;
    public event Action<bool> MenuOpened;
    public static UIManager instance;

    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI highscoreText;
    [SerializeField] GameObject menuParent;

    Dictionary<string, int> RewardDict;
    int currentScore = 0;
    int highscore = 0;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            if (isMenuOpeningAllowed)
            {
                MenuOpened.Invoke(true);
                menuParent.SetActive(true);
            }
        }

        else
        {
            MenuOpened.Invoke(false);
            menuParent.SetActive(false);
        }
    }

    public void Init(Dictionary<string, int> RewardDict)
    {
        this.RewardDict = RewardDict;
        TargetManager.instance.observer.HitResponse += UpdateScore;
        scoreText.text = $"Score: {currentScore}";
        highscore = PlayerPrefs.GetInt("highscore");
    }

    public void ShowHighscore(bool show)
    {
        if (show)
        {
            highscoreText.text = $"Highscore: {highscore}\n\nTry to beat it!";
            highscoreText.gameObject.SetActive(true);
        }

        else highscoreText.gameObject.SetActive(false);
    }

    void UpdateScore(GameObject targ)
    {
        int reward = RewardDict[targ.name];
        currentScore += reward;
        scoreText.text = $"Score: {currentScore}";
        UpdateHighscore();        
    }

    void UpdateHighscore()
    {
        if (currentScore > highscore)
        {
            highscore = currentScore;
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooting
{
    public class Projectile : MonoBehaviour
    {
        public float lifetime;

        void OnEnable()
        {
            Deactivate(lifetime);
        }

        public void Deactivate(float seconds) => StartCoroutine(RemoveAfterSeconds(lifetime));

        IEnumerator RemoveAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            gameObject.SetActive(false);
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}

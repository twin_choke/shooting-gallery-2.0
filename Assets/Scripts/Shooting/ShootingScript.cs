﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooting
{
    public class ShootingScript : MonoBehaviour
    {
        [SerializeField] GameObject projectilePrefab;
        [SerializeField] GameObject projectileParent;
        [SerializeField] AudioClip throwSound;
        [SerializeField] float shootPower = 35f;
        [SerializeField] float shootInterval = 0.4F;
        [SerializeField] int projPoolCount = 6;

        [HideInInspector] public bool isShootingEnabled = false;

        float nextShoot;
        bool shoot;
        AudioSource audioSource;
        Queue<GameObject> ProjectilesQueue = new Queue<GameObject>();

        void Start()
        {
            for (int i = 0; i < projPoolCount; i++)
            {
                var projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity, projectileParent.transform);
                projectile.SetActive(false);
                projectile.GetComponent<Rigidbody>().isKinematic = true;
                ProjectilesQueue.Enqueue(projectile);
            }

            audioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            if (Input.GetButtonUp("Fire1") && Time.time > nextShoot && isShootingEnabled)
            {
                shoot = true;
                nextShoot = Time.time + shootInterval;
            }               
        }

        void FixedUpdate()
        {
            if (shoot)
            {
                var projectile = SpawnProjectile();
                Shoot(projectile);
                shoot = false;
            }
        }

        void Shoot(GameObject projectile)
        {
            projectile.transform.position = transform.position;
            Rigidbody shot = projectile.GetComponent<Rigidbody>();
            audioSource.Play();
            shot.isKinematic = false;
            projectile.SetActive(true);
            shot.AddForce(Camera.main.transform.forward * shootPower, ForceMode.Impulse);
        }

        GameObject SpawnProjectile()
        {
            // паттерн Object Pool
            GameObject projectile = ProjectilesQueue.Dequeue();
            ProjectilesQueue.Enqueue(projectile);
            return projectile;
        }
    }
}

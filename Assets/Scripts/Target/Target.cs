﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Targets
{
    public class Target : MonoBehaviour
    {
        /// <summary>
        /// Событие попадания снаряда по мишени. В качестве параметра передаются имя мишени. 
        /// </summary>
        public event Action<GameObject> Hit;

        [SerializeField] private int reward;
        [SerializeField] private Sprite icon;

        public int Reward => reward;
        public Sprite Icon => icon;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Projectile"))
            {
                Hit?.Invoke(gameObject);
                gameObject.SetActive(false);
            }
        }
    }
}
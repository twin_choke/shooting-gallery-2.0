﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Targets
{
    public class TargetCreate : MonoBehaviour
    {
        [SerializeField] [Range(1, 4)] int maxSceneTargets;
        [SerializeField] float distanceBetweenTargs;
        [HideInInspector] public GameObject[] SceneTargets { get; private set; }       

        List<GameObject> Prefabs;
        List<GameObject> TargetsPool = new List<GameObject>();
        TargetsObserver observer;

        public void Init(List<GameObject> Prefabs, TargetsObserver observer)
        {
            this.Prefabs = Prefabs;
            this.observer = observer;
        }

        public void SetSceneTargets()
        {
            GameObject targ;
            List<GameObject> _tempSceneTargs = new List<GameObject>(maxSceneTargets);

            if (maxSceneTargets % 2 != 0)
            {
                Vector3 centerTargPos = gameObject.transform.position;
                targ = RandomPrefabInstance(centerTargPos);
                _tempSceneTargs.Add(targ);
                observer.Subscribe(targ);

                Vector3 leftTargPos = centerTargPos;
                Vector3 rightTargPos = centerTargPos;

                for (int i = 1; i < maxSceneTargets;)
                {
                    leftTargPos = new Vector3(leftTargPos.x - distanceBetweenTargs, leftTargPos.y, leftTargPos.z);
                    targ = RandomPrefabInstance(rightTargPos);
                    _tempSceneTargs.Add(targ);
                    observer.Subscribe(targ);

                    rightTargPos = new Vector3(rightTargPos.x + distanceBetweenTargs, rightTargPos.y, rightTargPos.z);
                    targ = RandomPrefabInstance(rightTargPos);
                    _tempSceneTargs.Add(targ);
                    observer.Subscribe(targ);

                    i += 2;
                }
            }

            else
            {
                Vector3 parentPos = gameObject.transform.position;
                Vector3 leftTargPos = new Vector3(parentPos.x - distanceBetweenTargs * 0.5f, parentPos.y, parentPos.z);
                Vector3 rightTargPos = new Vector3(parentPos.x + distanceBetweenTargs * 0.5f, parentPos.y, parentPos.z);

                for (int i = 0; i < maxSceneTargets;)
                {
                    targ = RandomPrefabInstance(leftTargPos);
                    leftTargPos = new Vector3(leftTargPos.x - distanceBetweenTargs, leftTargPos.y, leftTargPos.z);
                    _tempSceneTargs.Add(targ);
                    observer.Subscribe(targ);

                    targ = RandomPrefabInstance(rightTargPos);
                    rightTargPos = new Vector3(rightTargPos.x + distanceBetweenTargs, rightTargPos.y, rightTargPos.z);
                    _tempSceneTargs.Add(targ);
                    observer.Subscribe(targ);

                    i += 2;
                }
            }

            TargetsPool.AddRange(_tempSceneTargs);
            SceneTargets = _tempSceneTargs.ToArray();
        }

        // Задействовать при рефакторинге
        GameObject RandomPrefabInstance(Vector3 position)
        {
            GameObject targ;
            GameObject pref = RandomPrefab(Prefabs);
            targ = Instantiate(pref, position, Quaternion.identity, gameObject.transform);
            targ.name = pref.name;
            return targ;
        }

        // Ленивая инициализация мишеней.
        // Сначала запрошенный объект (в неактивном состоянии) ищется на сцене, затем в пуле всех ранее созданных мишеней, если нигде не найден - создается новый.
        public void AddToScene(string targetName)
        {
            IEnumerable<GameObject> disabledTargets = SceneTargets.Where(targ => !targ.activeSelf);
            if (!disabledTargets.Any())
                return;

            var disabledOnScene = disabledTargets.FirstOrDefault(targ => targetName.Equals(targ.name, System.StringComparison.Ordinal));
            if (disabledOnScene != null)
            {
                disabledOnScene.SetActive(true);
                observer.Subscribe(disabledOnScene);
                return;
            }

            else
            {
                int freePlaceIndex = System.Array.FindIndex(SceneTargets, targ => !targ.activeSelf);
                Vector3 scenePosition = SceneTargets[freePlaceIndex].transform.position;
                var poolTarg = TargetsPool.FirstOrDefault(targ => targetName.Equals(targ.name, System.StringComparison.Ordinal) && !targ.activeSelf);

                if (poolTarg != null)
                {
                    poolTarg.transform.position = scenePosition;
                    SceneTargets[freePlaceIndex] = poolTarg;
                    poolTarg.SetActive(true);
                    observer.Subscribe(poolTarg);
                }

                else
                {
                    var prefabToInst = Prefabs.FirstOrDefault(pref => targetName.Equals(pref.name, System.StringComparison.Ordinal));
                    var newTarg = Instantiate(prefabToInst, scenePosition, Quaternion.identity);
                    SceneTargets[freePlaceIndex] = newTarg;
                    newTarg.name = prefabToInst.name;

                    TargetsPool.Add(newTarg);
                    observer.Subscribe(newTarg);
                }                             
            }            
        }

        GameObject RandomPrefab(List<GameObject> Prefabs) => Prefabs[Random.Range(0, Prefabs.Count)];
    }
}

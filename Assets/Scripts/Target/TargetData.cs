﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Targets
{
    public class TargetData
    {
        public string targetName;
        public int reward;
        public string iconName;
    }

    [XmlRoot("TargetCollection")]
    public class TargetDataContainer
    {
        [XmlArray("Targets"), XmlArrayItem("Target")]
        public List<TargetData> TargetsDataList = new List<TargetData>();
    }
}
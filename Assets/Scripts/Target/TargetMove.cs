﻿using UnityEngine;

namespace Targets
{
    public class TargetMove : MonoBehaviour
    {
        [HideInInspector] public GameObject[] targets;

        [SerializeField] Transform noTargetsCollider;
        [SerializeField] BorderCollide leftBoundary;
        [SerializeField] BorderCollide rightBoundary;
        [SerializeField] float moveSpeed;

        Vector3 direction = Vector3.right;

        private void Start()
        {
            leftBoundary.TargetCollide += OnBorderCollisionEnter;
            rightBoundary.TargetCollide += OnBorderCollisionEnter;
        }

        public void Init(GameObject[] targets)
        {
            this.targets = targets;          
        }

        public void MovePosition()
        {
            for (int i = 0; i < targets.Length; i++)
            {
                Vector3 newPosition = targets[i].transform.position + direction * moveSpeed * Time.fixedDeltaTime;
                targets[i].transform.position = newPosition;           
            }

            noTargetsCollider.position = noTargetsCollider.position + direction * moveSpeed * Time.fixedDeltaTime;
        }

        void OnBorderCollisionEnter(Vector3 direction) => this.direction = direction;      
    }
}
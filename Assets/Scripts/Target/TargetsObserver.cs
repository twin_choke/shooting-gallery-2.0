﻿using System;
using UnityEngine;

namespace Targets
{
    public class TargetsObserver
    {
        public event Action<GameObject> HitResponse;

        void OnTargetHit(GameObject targ)
        {
            HitResponse?.Invoke(targ);
            Unsubscribe(targ);
        }

        public void Subscribe(GameObject targ) => targ.GetComponent<Target>().Hit += OnTargetHit;

        void Unsubscribe(GameObject targ) => targ.GetComponent<Target>().Hit -= OnTargetHit;
    }
}
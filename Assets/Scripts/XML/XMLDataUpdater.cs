﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Targets;

namespace IO
{
    [CreateAssetMenu(fileName = "XMLDataUpdater", menuName = "XMLDataUpdater")]
    public class XMLDataUpdater : ScriptableObject
    {
        [SerializeField] private List<GameObject> Targets = new List<GameObject>();

        public void OnValidate()
        {
            TargetDataContainer container = new TargetDataContainer();

            if (Targets.Any())
            {
                foreach (var targObject in Targets)
                {
                    TargetData data = new TargetData();
                    Target targ = targObject.GetComponent<Target>();
                    if (targ == null)
                    {
                        Targets.Remove(targObject);
                        Debug.Log($"{targObject.name} has no Target script attached!");
                        return;
                    }

                    data.targetName = targObject.name;
                    if (targ.Icon != null) // почему-то не работает упрощенная проверка targ.Icon?.name
                        data.iconName = targ.Icon.name;
                    data.reward = targ.Reward;

                    container.TargetsDataList.Add(data);
                }

                Debug.Log("XML file successfully updated!");
            }

            else Debug.Log("XML file is now empty!");

            XMLSerializer.Save(container, Application.streamingAssetsPath + "/TextData/TargetsData.xml");
        }
#if UNITY_EDITOR
        [CustomEditor(typeof(XMLDataUpdater))]
        public class ObjectBuilderEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();

                XMLDataUpdater myScript = (XMLDataUpdater)target;
                if (GUILayout.Button("Update XML data"))
                {
                    myScript.OnValidate();
                }
            }
        }
#endif
    }
}
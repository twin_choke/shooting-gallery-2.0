﻿using System.Xml.Serialization;
using System.IO;
using Targets;

namespace IO
{
    public static class XMLSerializer
    {
        public static void Save(TargetDataContainer container, string path)
        {
            var serializer = new XmlSerializer(typeof(TargetDataContainer));
            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, container);
            }
        }

        public static TargetDataContainer Load(string path)
        {
            var deserializer = new XmlSerializer(typeof(TargetDataContainer));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return deserializer.Deserialize(stream) as TargetDataContainer;
            }
        }
    }
}
